#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <list>
#include <vector>
#include <ctime>
#include <cstdio>
#include <cmath>
#include <dirent.h>

using namespace std;

#if not defined USING_LIST
#define USING_ARRAY
#endif

const string root_color_str = "RC";
const string num_colors_str = "NC";
const string output_folder = "output";

class KTree {
  int k; // the number of tree level
  int r; // the number of (k-1)-tree, (k-2)-tree, ...
  int n; // the number of nodes
#if defined USING_ARRAY
  bool **adj; // 2D array
#else
  list<int> *adj; // a dynamic array of adjacency lists
#endif
public:
  KTree(int k, int r) {
	this->k = k;
	this->r = r;
	n = pow(r+1, k-1);
#if defined USING_ARRAY
	adj = new bool *[n];
	for (int i = 0; i < n; i++)
	  adj[i] = new bool[n];
	
	// init adjacent array
	for (int i = 0; i < n; i++)
	  for (int j = 0; j < n; j++)
		adj[i][j] = 0;
#else
	adj = new list<int>[n];
#endif
	int root = 0;
	generate(k, root);
  }
  ~KTree() {
#if defined USING_ARRAY
	for (int i = 0; i < n; i++)
	  delete [] adj[i];
#endif
	delete [] adj;
  }
  
  // Generate a KTree
  void generate(int k, int &node);

  // Add new edge to the graph
  void add_edge(int u, int v);

  // Coloring the graph
  int *online_coloring(vector<int> vertices_order);
  
  // Display edges of the graph
  void display();
};

//-------------------------------------------------------
/// Generate K-Tree
/// @param k: the level of the graph
/// @param node: the root of the graph
//-------------------------------------------------------
void KTree::generate(int k, int &node) {
  if (k == 1)
	return;
  int root = node;
  for (int i = 1; i < k; i++) {
	for (int j = 0; j < r; j++) {
	  node += 1;
	  this->add_edge(root, node);
	  generate(k-i, node);
	}
  }
}

//--------------------------------------------------
/// Add new edge to the graph
/// @param u, v: two vertices of the edge
//--------------------------------------------------
void KTree::add_edge(int u, int v) {
#if defined USING_ARRAY
  adj[u][v] = 1;
  adj[v][u] = 1;
#else
  adj[u].push_back(v);
  adj[v].push_back(u);
#endif
}

//-------------------------------------------------------
/// Coloring the graph
/// @param vertices_oder: the order of visible vertices
/// @return n_color: the number of needed colors
//-------------------------------------------------------
int *KTree::online_coloring(vector<int> vertices_order) {
  int *colors = new int[n];
  // Assign the first color to the first vertex
  colors[vertices_order[0]] = 0;

  // Current and visible vertex
  int vertex, visible_vertex;
  for (int cur = 1; cur < n; cur++) {
	vertex = vertices_order[cur];

	// A temporary array stores the available colors
	// using to check if a color is assigned to any adjacent vertex
	bool available_colors[cur+1];
	for (int i = 0; i <= cur; i++)
	  available_colors[i] = false;

	// Assign a color to current vertex
	// by observing colors of all visible adjacent vertices
	for (int pre = cur-1; pre >= 0; pre--) {
	  visible_vertex = vertices_order[pre];

#if defined USING_ARRAY
	  if (adj[vertex][visible_vertex] == 1)
		available_colors[ colors[visible_vertex] ] = true;
#else // USING_LIST
	  list<int>::iterator it;
	  for (it = adj[vertex].begin(); it != adj[vertex].end(); it++)
		if (*it == visible_vertex)
		  available_colors[ colors[visible_vertex] ] = true;
#endif
	  // Find the first available color
	  int c;
	  for (c = 0; c <= cur; c++)
		if (available_colors[c] == false)
		  break;
	  colors[vertex] = c;
	}
  }
  return colors;
}

//--------------------------------------------------
/// Display the graph
//--------------------------------------------------
void KTree::display() {
  for (int vertex = 0; vertex < n; vertex++) {
#if defined USING_ARRAY
	for (int i = 0; i < n; i++)
	  if (adj[vertex][i] == 1)
		cout << vertex << " " << i << endl;
#else
	list<int>::iterator it;
	for (it = adj[vertex].begin(); it != adj[vertex].end(); it++)
	  cout << vertex << " " << *it << endl;
#endif
  }
}

//-----------------------------------------------------------------------
/// Run online coloring for small k
/// @param k, r: parameters of KTree
/// @param t: the number of tries
/// @param permutation: order of visible nodes
/// @param num_colors_hist: histogram of the number of needed colors
/// @param root_color_hist: histogram of the color of the root
//-----------------------------------------------------------------------
void run_online_coloring(int k, int r, int t, vector<int> permutation, 
						 int **num_colors_hist, int **root_color_hist) {
  int n = permutation.size();
  KTree *tree = new KTree(k, r);
#if defined DEBUG
  cout << "List of tree edges" << endl;
  tree->display();
#endif
  for (int i = 0; i < t; i++) {
	random_shuffle(permutation.begin(), permutation.end());
	int *colors = tree->online_coloring(permutation);
		
	int n_colors = 0;
	for (int j = 0; j < n; j++)
	  if (colors[j]+1 > n_colors)
		n_colors = colors[j]+1;

#if defined DEBUG
	cout << "TRY = " << i << endl;
	for (int x = 0; x < n; x++)
	  cout << permutation[x] << " " << colors[permutation[x]] << endl;
	cout << "NUM_COLORS = " << n_colors << endl;
#endif
	num_colors_hist[k][n_colors]++;
	root_color_hist[k][colors[0]]++;
	delete [] colors;
  }
  delete tree;
}

//--------------------------------------------------------------------
/// Run estimation for bigger k based on small k
/// @param k, r: parameters of KTree
/// @param t: the number of tries
/// @param permutation: order of visible nodes
/// @param num_colors_hist: histogram of the number of needed colors
/// @param root_color_hist: histogram of the color of the root
//--------------------------------------------------------------------
void run_estimation(int k, int r, int t, vector<int> permutation, 
					int **num_colors_hist, int **root_color_hist) {
  for (int i = 0; i < t; i++) {
	random_shuffle(permutation.begin(), permutation.end());
	
	bool available_colors[k];
	for (int m = 0; m < k; m++)
	  available_colors[m] = false;
	
	int root_color = -1;
	for (vector<int>::iterator it = permutation.begin(); it != permutation.end(); ++it) {		  
	  // if the node is the root of tree,
	  // check all visible nodes' colors to find available color
	  if (*it == 0) {
		for (root_color = 0; root_color < k; root_color++)
		  if (available_colors[root_color] == false)
			break;
		available_colors[root_color] = true;
		root_color_hist[k][root_color]++;
	  }
	  // if the node is not the root of tree, 
	  // choose a random previous result and check that color with root's color
	  else {
		int color = -1;
		int k_ = k - ceil(*it * 1.0 / r);
		int random = rand() % t;
		int sum = 0;
		for (color = 0; color < k_; color++) {
		  sum += root_color_hist[k_][color];
		  if (random < sum)
			break;
		}
		if (color == root_color) {
		  color += 1;
		}
		available_colors[color] = true;
	  }
	}

	int n_colors = 0;
	for (int x = 0; x < k; x++) {
	  if (available_colors[x] == true)
		n_colors++;
	}
	num_colors_hist[k][n_colors]++;
  }
}

//--------------------------------------------------------------------
/// Write result to file
/// @param k, r: parameters of KTree
/// @param t: the number of tries
/// @param m: max k to run online_coloring
/// @param num_colors_hist: histogram of the number of needed colors
/// @param root_color_hist: histogram of the color of the root
//--------------------------------------------------------------------
void outputToFile(int k, int r, int t, int m,
				  int **num_colors_hist, int **root_color_hist) {
  char filename[128];
  snprintf(filename, sizeof(filename), "k%d_r%d_t%d_m%d.txt", k, r, t, m);
  cout << "Output to file: " << output_folder << "/" << filename << endl;
  fstream fs;
  fs.open((output_folder + "/" + filename).c_str(), fstream::out);
  for (int i = 1; i < k+1; i++)
	for (int j = 1; j < i+1; j++)
	  if (num_colors_hist[i][j] > 0)
		fs << i << " " << num_colors_str << " " << j << " " << num_colors_hist[i][j] << endl;

  for (int i = 1; i < k+1; i++)
	for (int j = 0; j < i+1; j++)
	  if (root_color_hist[i][j] > 0)
		fs << i << " " << root_color_str << " " << j << " " << root_color_hist[i][j] << endl;
  fs.close();
}

//--------------------------------------------------------------------
/// Read previous result from file
/// @param k, r: parameters of KTree
/// @param t: the number of tries
/// @param m: max k to run online_coloring
/// @param num_colors_hist: histogram of the number of needed colors
/// @param root_color_hist: histogram of the color of the root
/// @return k_max if can read from file
///             2 if can't read
//--------------------------------------------------------------------
int readCachedResult(int k, int r, int t, int m,
					 int **num_colors_hist, int **root_color_hist) {
  DIR *dir;
  dirent *pdir;
  int k_max = 1;

  dir = opendir(("./" + output_folder).c_str());
  int k_temp, r_temp, t_temp, m_temp;
  string filename, filename_temp;
  while (pdir = readdir(dir)) {
	filename_temp = pdir->d_name;
	k_temp = r_temp = t_temp = m_temp = 0;
	sscanf(filename_temp.c_str(), "k%d_r%d_t%d_m%d", &k_temp, &r_temp, &t_temp, &m_temp);
	if (r == r_temp && t == t_temp && m == m_temp)
	  if (k_temp > k_max) {
		k_max = k_temp;
		filename = filename_temp;
	  }
  }
  if (k_max == 1)
	return 2;

  fstream fs;
  fs.open((output_folder + "/" + filename).c_str(), fstream::in);

  int i, j;
  string name;
  int frequent;
  while (!fs.eof()) {
	fs >> i >> name >> j >> frequent;
	if (i <= k && j <= k) {
	  if (name.compare(root_color_str) == 0)
		root_color_hist[i][j] = frequent;
	  if (name.compare(num_colors_str) == 0)
		num_colors_hist[i][j] = frequent;
	}
  }
  return k_max + 1;
}

//--------------------------------------------------------------------
/// Process parameters
/// @param argc, argv: input parameters
/// @param k, r: parameters of KTree
/// @param t: the number of tries
/// @param m: max k to run online_coloring
/// @return true if successful
///         false otherwise
//--------------------------------------------------------------------
bool processInput(int argc, char *argv[], int &k, int &r, int &t, int &m) {
  if (argc >= 3) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> k))
      return false;
    istringstream iss2( argv[2] );
    if (!(iss2 >> r))
      return false;
  }
  else {
	return false;
  }
  if (argc >= 4) {
	istringstream iss3( argv[3] );
    if (!(iss3 >> t))
	  return false;
  }
  if (argc >= 5) {
	istringstream iss4( argv[4] );
    if (!(iss4 >> m))
	  return false;
  }
  return true;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int k, r; // tree parameters
  int tries = 1000, max_k = 5; // number of tries, max k to run online_coloring

  // process input
  if (processInput(argc, argv, k, r, tries, max_k) == false)
	return 0;

  // table stores histogram of the number of needed colors
  int **num_colors_hist = new int*[k+1];
  for (int i = 0; i < k+1; i++)
	num_colors_hist[i] = new int[k+1];

  // table stores histogram of the root's color
  int **root_color_hist = new int*[k+1];
  for (int i = 0; i < k+1; i++)
	root_color_hist[i] = new int[k+1];

  // init these two tables
  for (int i = 0; i < k+1; i++) {
	for (int j = 0; j < k+1; j++) {
	  num_colors_hist[i][j] = 0;
	  root_color_hist[i][j] = 0;
	}
  }
  num_colors_hist[1][1] = tries;
  root_color_hist[1][0] = tries/2;
  root_color_hist[1][1] = tries - root_color_hist[1][0];

  // read previous result from file
  int temp = readCachedResult(k, r, tries, max_k, num_colors_hist, root_color_hist);

  vector<int> permutation1; // use for online_coloring
  vector<int> permutation2; // use for estimation
  for (int k_index = temp; k_index <= k; k_index++) {

#if defined DEBUG
	cout << "K_INDEX = " << k_index << endl;
#endif

	// if k is small, run online_coloring algorithm
	if (k_index < max_k) {
	  int n = pow(r+1, k_index-1);
	  for (int i = permutation1.size(); i < n; i++)
		permutation1.push_back(i);
	
	  run_online_coloring(k_index, r, tries, permutation1, num_colors_hist, root_color_hist);
	}

	// if k is large, run estimation based on previous results
	else {
	  int n = 1 + r * (k_index-1);
	  for (int i = permutation2.size(); i < n; i++)
		permutation2.push_back(i);

	  run_estimation(k_index, r, tries, permutation2, num_colors_hist, root_color_hist);
	}
  }

  // print the result
  outputToFile(k, r, tries, max_k, num_colors_hist, root_color_hist);

  // free memory
  for (int i = 0; i < k+1; i++)
	delete [] num_colors_hist[i];
  delete [] num_colors_hist;

  for (int i = 0; i < k+1; i++)
	delete [] root_color_hist[i];
  delete [] root_color_hist;

  return 0;
}
