#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <list>
#include <vector>
#include <ctime>
#include <cstdio>
#include <cmath>

using namespace std;

//#define DEBUG

#if not defined USING_LIST
#define USING_ARRAY
#endif

#if not defined NUM_TRIAL
#define NUM_TRIAL 1000
#endif

class Graph {
  int n; // number of vertices
#if defined USING_ARRAY
  bool **adj; // 2D array
#else
  list<int> *adj; // a dynamic array of adjacency lists
#endif
  int *colors; // an array stores color of each vertex
public:
  Graph(int n) {
	this->n = n;
#if defined USING_ARRAY
	adj = new bool *[n];
	for (int i = 0; i < n; i++)
	  adj[i] = new bool[n];
	for (int i = 0; i < n; i++)
	  for (int j = 0; j < n; j++)
		adj[i][j] = 0;
#else
	adj = new list<int>[n];
#endif
	colors = new int[n];
  }
  ~Graph() {
#if defined USING_ARRAY
	for (int i = 0; i < n; i++)
	  delete [] adj[i];
#endif
	delete [] adj;
	delete [] colors;
  }
  
  // Add new edge to the graph
  void add_edge(int u, int v);

  // Coloring the graph
  int online_coloring(vector<int> vertices_order);
  
  // Validate two neighbors have different colors
  bool validate();

  // Display edges of the graph
  void display();
};

//--------------------------------------------------
/// Add new edge to the graph
/// @param u, v: two vertices of the edge
//--------------------------------------------------
void Graph::add_edge(int u, int v) {
#if defined USING_ARRAY
  adj[u][v] = 1;
  adj[v][u] = 1;
#else
  adj[u].push_back(v);
  adj[v].push_back(u);
#endif
}

//-------------------------------------------------------
/// Coloring the graph
/// @param vertices_oder: the order of visible vertices
/// @return n_color: the number of needed colors
//-------------------------------------------------------
int Graph::online_coloring(vector<int> vertices_order) {
  // Assign the first color to the first vertex
  colors[vertices_order[0]] = 0;

  // Current and visible vertex
  int vertex, visible_vertex;
  for (int cur = 1; cur < n; cur++) {
	vertex = vertices_order[cur];

	// A temporary array stores the available colors
	// using to check if a color is assigned to any adjacent vertex
	bool available_colors[cur+1];
	for (int i = 0; i <= cur; i++)
	  available_colors[i] = false;

	// Assign a color to current vertex
	// by observing colors of all visible adjacent vertices
	for (int pre = cur-1; pre >= 0; pre--) {
	  visible_vertex = vertices_order[pre];

#if defined K_TREE
#if defined USING_ARRAY
	  if (adj[vertex][visible_vertex] == 1)
		available_colors[ colors[visible_vertex] ] = true;
#else // USING_LIST
	  list<int>::iterator it;
	  for (it = adj[vertex].begin(); it != adj[vertex].end(); it++)
		if (*it == visible_vertex)
		  available_colors[ colors[visible_vertex] ] = true;
#endif
#else
	  // Binary Tree
	  if (visible_vertex == 2*vertex+1)
		available_colors[ colors[2*vertex+1] ] = true;
	  else if (visible_vertex == 2*vertex+2)
		available_colors[ colors[2*vertex+2] ] = true;
	  else if (vertex%2 != 0 && visible_vertex == (vertex-1)/2)
		available_colors[ colors[(vertex-1)/2] ] = true;
	  else if (vertex%2 == 0 && visible_vertex == (vertex-2)/2)
		available_colors[ colors[(vertex-2)/2] ] = true;
#endif
	  // Find the first available color
	  int c;
	  for (c = 0; c <= cur; c++)
		if (available_colors[c] == false)
		  break;
	  colors[vertex] = c;
	}
  }

  int n_colors = 0;
  for (int u = 0; u < n; u++) {
#if defined DEBUG
	cout << "Vertex " << vertices_order[u] << " ---> Color " 
		 << colors[vertices_order[u]] << endl;
#endif
	if (colors[u] > n_colors)
	  n_colors = colors[u];
  }
  return n_colors + 1;
}

//------------------------------------------------------
/// Validate coloring algorithm
/// @return false if any two neighbors have same color
///         true  otherwise
//------------------------------------------------------
bool Graph::validate() {
  for (int vertex = 0; vertex < n; vertex++) {
#if defined USING_ARRAY
	for (int i = 0; i < n; i++)
	  if (adj[vertex][i] == 1 && colors[vertex] == colors[i])
		return false;
#else
	list<int>::iterator it;
	for (it = adj[vertex].begin(); it != adj[vertex].end(); it++)
	  if (colors[vertex] == colors[*it])
		return false;
#endif
  }
  return true;
}

//--------------------------------------------------
/// Display the graph
//--------------------------------------------------
void Graph::display() {
  for (int vertex = 0; vertex < n; vertex++) {
#if defined USING_ARRAY
	for (int i = 0; i < n; i++)
	  if (adj[vertex][i] == 1)
		cout << vertex << " " << i << endl;
#else
	list<int>::iterator it;
	for (it = adj[vertex].begin(); it != adj[vertex].end(); it++)
	  cout << vertex << " " << *it << endl;
#endif
  }
}

//--------------------------------------------------
/// Generate a binary tree
/// @param n: the number of nodes
//--------------------------------------------------
Graph *generateBinaryTree(int n) {
  Graph *g = new Graph(n);
  for (int i = 0; i < n; i++) {
	if (2*i+1 < n)
	  g->add_edge(i, 2*i+1);
	if (2*i+2 < n)
	  g->add_edge(i, 2*i+2);
  }
  return g;
}

//-------------------------------------------------------
/// Generate K-Tree
/// @param g: the generated graph
/// @param k: the level of the graph
/// @param r: the number of (k-1)-tree, (k-2)-tree, ...
/// @param node: the root of the graph
//-------------------------------------------------------
void generateKTree(Graph *g, int k, int r, int &node) {
  if (k == 1)
	return;
  int root = node;
  for (int i = 1; i < k; i++) {
	for (int j = 0; j < r; j++) {
	  node += 1;
	  g->add_edge(root, node);
	  generateKTree(g, k-i, r, node);
	}
  }
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int n = 0;
  if (argc >= 2) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return 0;
  }

  char filename[32];
#if defined K_TREE
  int root = 0;
  int k = n;
  int r = 1;
  if (argc >= 3) {
    istringstream iss2( argv[2] );
    if (!(iss2 >> r))
      r = 1;
  }
  n = pow(1+r, k-1);
  Graph *g = new Graph(n);
  generateKTree(g, k, r, root);

  snprintf(filename, sizeof(filename), "k%d_r%d_tree.txt", k, r);
#else
  Graph *g = generateBinaryTree(n);
  snprintf(filename, sizeof(filename), "b_tree_%d.txt", n);
#endif
#if defined DEBUG
  g->display();
#endif

  vector<int> permutation;
  for (int i = 0; i < n; i++)
    permutation.push_back(i);

  fstream fs;
  fs.open (filename, std::fstream::in | std::fstream::out | std::fstream::app);

  int n_colors, result = 1;
  for (int i = 0; i < NUM_TRIAL; i++) {
	random_shuffle(permutation.begin(), permutation.end());
	n_colors = g->online_coloring(permutation);
#if defined VALIDATE_RESULT
	result = g->validate();
#endif
#if defined DEBUG
	cout << n_colors << ", " << result << endl;
#endif
	fs << n_colors << ", " << result << endl;
  }
  fs.close();

  delete g;

  return 0;
}
