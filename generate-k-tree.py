import networkx as nx
import matplotlib.pyplot as plt

G = nx.Graph()
G.add_node(0)
id = 0
def generate_k_tree(k, r):
    global id
    if k == 1:
        return
    for i in range(1, k):
        id += 1
        G.add_node(id)
        G.add_edge(r, id)          
        generate_k_tree(k-i, id)

generate_k_tree(5, 0)
nx.write_edgelist(G , 'k-tree.txt')
nx.draw_circular(G)
#nx.draw_spectral(G)
plt.show()

