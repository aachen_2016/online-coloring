# Online coloring
A coloring of a graph G is an assignment of colors to the vertices of G such that no two adjacent vertices share the same color.

An online coloring is a coloring algorithm that immediately colors the vertices of a graph G taken from a list without looking ahead
or changing colors already assigned.

## Problem
In this assignment, we try to run online algorithm for k-tree (TODO: k-tree definition with parameter r) to find out: with specific k, which r should be so that 50% of chance the online algorithm needs k colors.

## Implementation
###graph-coloring.cpp
Using `random_shuffle` algorithm to create different orders of vertices.

Online algorithm uses first fit strategy.

###k-tree-coloring.cpp
Because online algorithm takes a lot of time when k is large, we use dynamic programming to solve.

- Run online algorithm with small k
- Run estimation for large k based on results of small k

## Running
### Build
```
g++ [-pipe -O2 -std=c++11] [-D DEBUG] [-D NUM_TRIAL=10] [-D USING_LIST] [-D K_TREE] -o graph-coloring graph-coloring.cpp 
```
Explanation

- `-D DEBUG` Show output step-by-step
- `-D NUM_TRIAL=10` Set the number of running game for each k, by default `NUM_TRIAL=1000`
- `-D USING_LIST` Set using adjacent list to represent graph, by default `USING_ARRAY`
- `-D K_TREE` Set type of graph, by default `BINARY_TREE`

```
g++ [-pipe -O2 -std=c++11] [-D DEBUG] [-D USING_LIST] -o k-tree-coloring k-tree-coloring.cpp 
```
Explanation

- `-D DEBUG` Show output step-by-step
- `-D USING_LIST` Set using adjacent list to represent graph, by default `USING_ARRAY`

### Run
For binary tree, you can use this command (n is the number of nodes):
```
./graph-coloring n
```

For k-tree,
```
./graph-coloring k r
```
```
./k-tree-coloring k r t m
```
Explanation

- `k, r` the parameters of k-tree
- `t` the number of tries, by default `t = 1000`
- `m` max k to run online algorithm, by default `m = 5`

## Result
Using `draw-diagram.py` to show the relationship between k and r so that 50% of chance online coloring needs k colors.

Using `draw-histogram.py` to show the histogram of each r
```
python draw-histogram.py k
```
