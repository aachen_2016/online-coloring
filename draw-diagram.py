import matplotlib.pyplot as plt

list_name = []
for i in range(1, 18):
    list_name.append("k1000_r%d_t1000_m5.txt" % i)

data = [[[0 for n in range(1001)] for k in range(1001)] for r in range(len(list_name))]
i = 0
for filename in list_name:
    with open("output/" + filename) as textFile:
        for line in textFile:
            (k, name, n, f) = line.split()
            if name == 'NC':
                data[i][int(k)][int(n)] = int(f)
        i += 1

r = []
for k in range(1, 1001):
    i = 0
    while (i < len(list_name) and data[i][k][k] < 500):
        i += 1
    r.append(i+1)

k = [i+1 for i in range(1000)]

plt.plot(k, r)
plt.xlabel('k')
plt.ylabel('r')
plt.grid(True)
plt.show()
