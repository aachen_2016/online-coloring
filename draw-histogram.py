import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import sys

list_name = []
for i in range(1, 18):
    list_name.append("k1000_r%d_t1000_m5.txt" % i)

data = [[[0 for n in range(1001)] for k in range(1001)] for r in range(len(list_name))]
i = 0
for filename in list_name:
    with open("output/" + filename) as textFile:
        for line in textFile:
            (k, name, n, f) = line.split()
            if name == 'NC':
                data[i][int(k)][int(n)] = int(f)
        i += 1

k = int(sys.argv[1])
r = 0
while (r < len(list_name) and data[r][k][k] < 500):
    r += 1
r += 1

plt.figure(1)
num_bar = 7

for i in range(1, 10):
    x = '%d%d' % (33, i)
    plt.subplot(int(x))
    
    t = r - 7 + i
    colors = []
    hist = []
    for j in range(k+1):
        if data[t][k][j] > 0:
            hist.append(data[t][k][j])
            colors.append(j)

    while len(colors) < num_bar:
        colors.insert(0, colors[0] - 1)
        hist.insert(0, 0)
    
    objects = tuple(colors)
    y_pos = np.arange(len(objects))
    plt.bar(y_pos, hist, align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0,1000))
    #plt.xlabel('No. colors')
    #plt.ylabel('Frequency')
    plt.title('r = ' + str(t))
 
plt.show()
